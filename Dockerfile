# ---- Base Node ---- #
FROM ubuntu:14.04 AS Base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu trusty main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libssl-dev git-core libboost-all-dev libdb4.8-dev libdb4.8++-dev libminiupnpc-dev pkg-config && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ---- #
FROM base as build
RUN git clone https://github.com/jimblasko/UnbreakableCoin-master.git /opt/unbreakablecoin && \
    cd /opt/unbreakablecoin/src && \
    make -f makefile.unix

# ---- Release ---- #
FROM ubuntu:14.04 as release
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu trusty main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y  libboost-all-dev libdb4.8 libdb4.8++ libminiupnpc-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN groupadd -r unbreakablecoin && useradd -r -m -g unbreakablecoin unbreakablecoin
RUN mkdir /data
RUN chown unbreakablecoin:unbreakablecoin /data
COPY --from=build /opt/unbreakablecoin/src/unbreakablecoind /usr/local/bin/
USER unbreakablecoin
VOLUME /data
EXPOSE 9336 9337
CMD ["/usr/local/bin/unbreakablecoind", "-conf=/data/unbreakablecoin.conf", "-server", "-txindex", "-printtoconsole"]